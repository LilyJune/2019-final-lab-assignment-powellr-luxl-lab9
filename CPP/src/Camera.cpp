/*
 * Camera.cpp
 *
 *  Created on: Nov 4, 2019
 *      Author: se3910
 */

#include "Camera.h"
//#include <cstdlib.h>

using namespace cv;

Camera::Camera(int width, int height, std::string threadName): RunnableClass(threadName) {
    this->capture = new VideoCapture(0);
    this->capture->set(CV_CAP_PROP_FRAME_WIDTH, width);
    this->capture->set(CV_CAP_PROP_FRAME_HEIGHT, height);
    this->capture->set(CV_CAP_PROP_FPS, 30);
    this->lastFrame = NULL;
}

Camera::~Camera() {
    delete this->lastFrame;
    delete this->capture;
}

void Camera::run() {
	if(!this->capture->isOpened()) {
	    printf("Failed to connect to the camera.\n");
	    exit(-1);
	}

	bool go = true;

	while(go) {
//		printf("no seg fault plz\n");
		bool resolved = this->capture->grab();
		if(resolved) {
			this->mtx.lock();
//			this->capture->grab();
			Mat* temp = new Mat();
			this->capture->retrieve(*temp);
			delete this->lastFrame;
			this->lastFrame = temp;
			go = this->keepGoing;
			this->mtx.unlock();
		} else {
			printf("failed to capture\n");
		}
	}
}

void Camera::shutdown() {
	this->mtx.lock();
	this->keepGoing = false;
	this->mtx.unlock();
}

Mat* Camera::takePicture() {
	this->mtx.lock();
	Mat * retVal = NULL;
	if (this->lastFrame != NULL && !this->lastFrame->empty()) {
		retVal = new Mat();
		this->lastFrame->copyTo(*retVal);
	}
	this->mtx.unlock();
	return retVal;
//	if(this->lastFrame->empty()) {
//		Mat* ret;
//	} else {
//		return nullptr;
//	}


}




