/**
 * @file NetworkCommands.h
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 * This file defines the commands that are to be sent over the network to the device.
 */

#ifndef NETWORKCOMMANDS_H_
#define NETWORKCOMMANDS_H_

/**
 * This segment defines the destination queues and devices for the given messages.
 */
#define MOTOR_CONTROL_DESTINATION (0x00000001)
#define NAV_CONTROL_DESTINATION   (0x00000002)
/**
 * This definition is for the diagnostic controller and the queue used to receive commands.
 */
#define DIAGNOSTIC_MANAGER_CONTROL_DESTINATION (0x00000003)
/**
 * This command indicates that the rest of the word being sent over the network is consumed by the speed setting.
 */
#define SPEEDDIRECTIONBITMAP  0x40000000

/**
 * These are definitions related to the control of the robot via the network.
 */
#define MOTORDIRECTIONBITMAP  0x20000000


/**
 * This definition indicates that there is a message related to collision management to be processed.
 */
#define COLLISIONBITMAP       0x08000000

/**
 * This value indicates that a collision has been sensed by a collision sensor.
 */
#define COLLISION_SENSED      0x0000A5A5

/**
 * This value indicates that the collision has been cleared.
 */
#define COLLISION_CLEARED     0x00005A5A

/**
 * This indicates the value that is or'ed with the MOTORDIRECTIONBITMAP to indicate that the direction of the robot is to be forward.
 */
#define  FORWARD  0x00000001
/**
 * This indicates the value that is or'ed with the MOTORDIRECTIONBITMAP to indicate that the direction of the robot is to be backward.
 */
#define  BACKWARD  0x00000002
/**
 * This indicates the value that is or'ed with the MOTORDIRECTIONBITMAP to indicate that the direction of the robot is to be left.
 */
#define  LEFT  0x00000004
/**
 * This indicates the value that is or'ed with the MOTORDIRECTIONBITMAP to indicate that the direction of the robot is to be right.
 */
#define  RIGHT  0x00000008
/**
 * This indicates the value that is or'ed with the MOTORDIRECTIONBITMAP to indicate that the robot is to stop.
 */
#define  STOP  0x00000010

/**
 * This definition is a combination of all motor directions, and can be used as a generic mask when commands are sent.
 */
#define MOTORDIRECTIONS (FORWARD | BACKWARD | LEFT | RIGHT| STOP)

/**
 * This definition defined the network message that will start the white calibration on the robot.
 */
#define START_WHITE_CALIBRATION (0x40000000)

/**
 * This definition defined the network message that will start the black calibration on the robot.
 */
#define START_BLACK_CALIBRATION (0x20000000)

/**
 * This definition defined the network message that will start the robot line following.
 */
#define START_LINE_FOLLOWING (0x10000000)

/**
 * This definition defined the network message that will stop the robot line following.
 */
#define STOP_LINE_FOLLOWING (0x08000000)


/**
 * This definition indicates that KP of the PID needs to be updated.
 */
#define  UPDATE_KP   (0x04000000)
/**
 * This definition indicates that KD of the PID needs to be updated.
 */

#define  UPDATE_KD   (0x02000000)

/**
 * This definition indicates that KI of the PID needs to be updated.
 */

#define  UPDATE_KI   (0x01000000)

/**
 * This command will reset the min and max distances of the diagnostic manager.
 */
#define DIAG_RESET_MIN_MAX_DISTANCES 0x40000000


/**
 * This set of commands defines the commands used for distance reading.
 */
#define DISTANCEREADINGBITMAP  (0x08000000)
/**
 * Read the current distance reading from the sensor.
 */
#define CURRENTREADINGBITMAP   (0x00800000)
/**
 * Obtain the average reading from the sensor.
 */
#define AVERAGEREADINGBITMAP   (0x00400000)
/**
 * Obtain the maximum reading from the sensor.
 */
#define MAXREADINGBITMAP       (0x00200000)
/**
 * Obtain the minimum reading from the sensor.
 */
#define MINREADINGBITMAP       (0x00100000)

/**
 * This definition will cause the diagnostic report to be issued on the robot.
 */
#define DIAGNOSTIC_REPORT_REQUEST (0x20000000)
/**
 * This will reset the diagnostic status of the robot.
 */
#define DIAGNOSTIC_RESET_REQUEST  (0x10000000)
#endif /* NETWORKCOMMANDS_H_ */
