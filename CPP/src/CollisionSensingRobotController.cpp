
#include "CollisionSensingRobotController.h"
#include "TaskRates.h"
#include "RobotCfg.h"
#include "NetworkCommands.h"
#include <iostream>

CollisionSensingRobotController::CollisionSensingRobotController(CommandQueue* queue, PWMManager& pwmInstance, int leftSensorPin, int rightSensorPin, std::string threadname) : RobotController(queue, pwmInstance, threadname) {
	this->lcs = new CollisionSensor(leftSensorPin, queue, "Left Collision Sensor", COLLISION_SENSING_TASK_RATE);
	this->rcs = new CollisionSensor(rightSensorPin, queue, "Right Collision Sensor", COLLISION_SENSING_TASK_RATE);
	this->robotHorn = new Horn(BUZZER, "Robot Horn", HORN_TASK_RATE);
    this->previousOperation = 0;
}

CollisionSensingRobotController::~CollisionSensingRobotController() {
	delete this->lcs;
	delete this->rcs;
	delete this->robotHorn;
}

void CollisionSensingRobotController::run() {
	while (this->keepGoing == true) {
		int command = this->referencequeue->dequeue();
//		std::cout << command << std::endl;

	// process motion commands
		if((command & MOTORDIRECTIONBITMAP) == MOTORDIRECTIONBITMAP) {
			std::cout << "motion command" << std::endl;
			int motionCommand = command & MOTORDIRECTIONS;
			this->processMotionControlCommand(motionCommand);
			if((motionCommand & BACKWARD) == BACKWARD) {
				std::cout << "pulse" << "\n";
				this->robotHorn->pulseHorn(3, 6); // pulse horn; values given in milliseconds
			} else {
				this->robotHorn->silenceHorn();
				std::cout << "stop\n";
			}
		} else if((command & SPEEDDIRECTIONBITMAP) == SPEEDDIRECTIONBITMAP) {
			std::cout << "speed command " << std::endl;
			this->processSpeedControlCommand(command - SPEEDDIRECTIONBITMAP);
		} else if((command & COLLISIONBITMAP) == COLLISIONBITMAP) {
			std::cout << "collision command" << std::endl;
			int collisionCommand = command - COLLISIONBITMAP;
			bool rightSensorObstructed = this->rcs->isObstructed();
			bool leftSensorObstructed = this->lcs->isObstructed();

			if(rightSensorObstructed) {
				std::cout << "Right sensor blocked" << std::endl;
			}

			if(leftSensorObstructed) {
				std::cout << "Left sensor blocked" << std::endl;
			}

			if(((this->currentOperation & FORWARD) == FORWARD || (this->previousOperation & FORWARD) == FORWARD) && (collisionCommand == COLLISION_SENSED)) {
//				this->previousOperation = this->currentOperation;
				if(rightSensorObstructed && leftSensorObstructed) {
					std::cout << "Right and Left Sensor Blocked -- Stopping" << std::endl;
					this->processMotionControlCommand(STOP);
					this->robotHorn->soundHorn();
				} else if(rightSensorObstructed && !leftSensorObstructed && this->previousOperation == 0) {
					this->processMotionControlCommand(RIGHT);
					this->previousOperation = command;
					this->robotHorn->pulseHorn(3, 6); // pulse horn; values given in milliseconds
					std::cout << "Right Sensor Blocked -- Moving back and left " << this->previousOperation << std::endl;
				} else if(!rightSensorObstructed && leftSensorObstructed && this->previousOperation == 0) {
					this->processMotionControlCommand(LEFT);
					this->previousOperation = command;
					std::cout << "Left Sensor Blocked -- Moving back and right" << this->previousOperation << std::endl;
					this->robotHorn->pulseHorn(3, 6); // pulse horn; values given in milliseconds
				}
			} else if((collisionCommand == COLLISION_CLEARED) && !rightSensorObstructed && !leftSensorObstructed && this->currentOperation != STOP) {
				std::cout << "All clear -- resuming movement" << std::endl;
				this->processMotionControlCommand(FORWARD);
				this->previousOperation = 0;
				this->robotHorn->silenceHorn();
			}
		}
	}
}

void CollisionSensingRobotController::stop() {
	RobotController::stop();
	this->lcs->stop();
	this->rcs->stop();
	this->robotHorn->stop();

}

void CollisionSensingRobotController::waitForShutdown() {
	this->lcs->waitForShutdown();
	this->rcs->waitForShutdown();
	this->robotHorn->waitForShutdown();

}

void CollisionSensingRobotController::startChildRunnables() {
	this->lcs->start(COLLISION_SENSING_TASK_PRIORITY);
	this->rcs->start(COLLISION_SENSING_TASK_PRIORITY);
	this->robotHorn->start(HORN_TASK_PRIORITY);
}
