/*
 * Horn.h
 *
 *  Created on: Oct 23, 2019
 *      Author: se3910
 */

#ifndef HORN_H_
#define HORN_H_

#include "GPIO.h"
#include "PeriodicTask.h"

class Horn: public PeriodicTask {
private:
	se3910RPi::GPIO* hornPin;
	int hornCount = -1;
	int length = 0;
	int repetitionTime = 1;

public:
	Horn(int gpioPin, std::string threadName, uint32_t taskRate);
	virtual ~Horn();
	void soundHorn();
	void pulseHorn(int length, int period);
	void silenceHorn();
	void taskMethod();
};

#endif /* HORN_H_ */
