/*
      * Camera.h
      * This class will use the OpenCV Video capture feature to capture images from the camera on the Raspberry Pi.
      * It is a runnable class,  as it has a thread that handles image capture.
      *  Created on: Jun 4, 2018
      *      Author: se3910
      */

     #ifndef CAMERA_H_
    #define CAMERA_H_

    #include "RunnableClass.h"
    #include <opencv2/opencv.hpp>
    #include <mutex>

    using namespace std;
    using namespace cv;

    class Camera: public RunnableClass {
    private:
    	/**
    	 * The video capture is an instance of the OpenCV Image capture class. It is instantiated in the constructor and used to capture images.
    	 */
        VideoCapture *capture;
        /**
         * This is the last frame that was captured by the camera. It is in an OpenCV Matrix format.
         */
        Mat * lastFrame;
        /**
         * This is a mutex within the camera class that prevents race conditions as the image is manipulated.
         */
        std::mutex mtx;
    public:
        /**
         * Construct a new instance of the camera class.
         * @param width This is the width of the natively captured images. This is the resolution the camera generates.
         * @param height This is the height of the natively captured images. This is the resolution the camera generates.
         * @param threadName This is the name of the thread that is to be used to run the image capture.
         */
        Camera(int width, int height, std::string threadName);
        /**
         * This is the destructor for the camera. It will delete all dynamically allocated objects.
         */
        virtual ~Camera();
/**
 * This is the main thread for the camera. It will do the following:
If we are unable to connect exit.
While the thread is to keep running: 2a. grab the next image from the camera. 2b. Retrieve the image into the last frame.
 */
        void run();
        /**
         * This method will shutdown the camera, preventing further pictures form being taken. It basically sets keepGoing to false.
         */
        void shutdown();
        /**
         * This method will return the next picture from the camera, following the algorithms described here:
If the last frame was empty, return NULL.
Instantiate a new matrix.
Copy the data from the last frame into the return frame.
Return the frame.
Note that this method will need to be properly synchronized with the run method to prevent race conditions.
         * @return The return will be a matrix of the picture that was last grabbed from the camera.
         */
        Mat* takePicture();
    };

    #endif /* CAMERA_H_ */






#if 0==1

/*
 * Camera.h
 *
 *  Created on: Jun 4, 2018
 *      Author: se3910
 */

#ifndef CAMERA_H_
#define CAMERA_H_

#include <opencv2/opencv.hpp>
#include <thread>
#include <mutex>

using namespace std;
using namespace cv;

class Camera {
private:
	VideoCapture *capture;
	Mat * lastFrame;
	std::mutex mtx;
	std::thread *myThread;
	bool keepGoing;
public:
	Camera();
	virtual ~Camera();
	void start();
	void run();
	void shutdown();
	Mat* takePicture();
};
#endif

#endif /* CAMERA_H_ */
