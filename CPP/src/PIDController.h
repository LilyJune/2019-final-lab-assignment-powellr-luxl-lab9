/**
 * @file PIDCOntroller.h
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE
 *
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 * This interface defines the interface for a PID controller class.  The PID controller class will control allow a system to be controlled using PID concepts.
 * The interface can be sued for any system which requires PID control.
 */

#ifndef PIDCONTROLLER_H_
#define PIDCONTROLLER_H_

class PIDController {
private:
	/**
	 * This is the constant value representing the proportional gain.
	 */
	double kp = 1.0;
	/**
	 * This is the value representing the integral gain.
	 */
	double ki = 0.0;
	/**
	 * This is the value which represents the derivative gain.
	 */
	double kd = 0.0;
	/**
	 * This is the set value for the controller.  It is the value that ideally will be returned by the system.
	 */
	double setValue = 0.0;

	/**
	 * This is the error value from the last reading.  It is stored to allow calculation of the change in error.
	 */
	double previousError = 0.0;
	/**
	 * This si the total error of the system, integrated over time.
	 */
	double totalError = 0.0;
	/**
	 * This is the current output based upon the set of calculations.
	 */
	double output = 0.0;

public:
	/**
	 * This is the default constructor.
	 */
	PIDController();
	/**
	 * This is the default destructor.
	 */
	virtual ~PIDController();
	/**
	 * This method will set the value that the PID controller is attempting to maintain.
	 * @param setValue This si the value that the system is to be set to.
	 */
	void setSetValue(double setValue);
	/**
	 * This method will set the proportional gain for the system.
	 * @param kp The proportional gain.
	 */
	void setKp(double kp);
	/**
	 * This method will set the integral gain for the system.
	 * @param ki This si the integral gain.
	 */
	void setKi(double ki);
	/**
	 *
	 * This method will set the derivative gain.
	 * @param kd This is the derivative gain.
	 */
	void setKd(double kd);
	/**
	 * This method will update the output value.
	 * @param currentValue This is the current, measured value from the system.
	 * @return This is the control value to feed into the system.
	 */
	double updateOutput(double currentValue);
	/**
	 * This method will obtain the output for the given system.
	 * @return The return is the output value.
	 */
	double getOutput();
	/**
	 * This method will reset the controller to the default values.
	 */
	void reset();
};

#endif /* PIDCONTROLLER_H_ */
