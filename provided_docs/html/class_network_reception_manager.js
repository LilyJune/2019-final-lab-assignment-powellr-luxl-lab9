var class_network_reception_manager =
[
    [ "NetworkReceptionManager", "class_network_reception_manager.html#a5aaeb678c632b72fdcb3435a1da5be54", null ],
    [ "~NetworkReceptionManager", "class_network_reception_manager.html#aaf06388d9c1f4e2facbe873d0c624579", null ],
    [ "getSocketID", "class_network_reception_manager.html#a25461cc56193896b1b7253f9b0ca9e19", null ],
    [ "run", "class_network_reception_manager.html#ad85673494de40f9e9522861195e3a682", null ],
    [ "stop", "class_network_reception_manager.html#a4b0e9146dc7f00a369afc5c5d01d83fd", null ],
    [ "portNumber", "class_network_reception_manager.html#ac36e49ec80dbd15f5ddb5dd7d381a36a", null ],
    [ "referencequeue", "class_network_reception_manager.html#a8f6b9935af9c33c169e2b724bb6ce441", null ],
    [ "server_fd", "class_network_reception_manager.html#adfe0628506c0c499d2b2a9a3397a469a", null ],
    [ "socketID", "class_network_reception_manager.html#a80c01aa1376785680656900dce4be5d7", null ]
];