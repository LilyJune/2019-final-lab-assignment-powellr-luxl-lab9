var class_collision_sensor =
[
    [ "CollisionSensor", "class_collision_sensor.html#a506f8d4869b7bc88e497361353b9cbe5", null ],
    [ "~CollisionSensor", "class_collision_sensor.html#a4b095b852202823d5ceb069414db0784", null ],
    [ "isObstructed", "class_collision_sensor.html#a450d2fe2d0801c25e161ec0b5b28cae7", null ],
    [ "taskMethod", "class_collision_sensor.html#ae96a0a0dc18a27d68d856d362d674c53", null ],
    [ "obstructed", "class_collision_sensor.html#aa5d4e24a1fa955588736a7b0c2d4743f", null ],
    [ "queue", "class_collision_sensor.html#aeaf5ec84296983733cc639efec9d91d9", null ],
    [ "sensorPin", "class_collision_sensor.html#a0a22c88868849d9684b5e66af198d990", null ]
];